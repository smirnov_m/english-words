package com.generationstock.config;

public class ApplicationConstants {
    public static final String FRONTEND_URL = "http://localhost:4200";
    public static final String BACKEND_URL = "http://localhost:8080";
//    public static final String FRONTEND_URL = "https://yandex.ru";
//    public static final String BACKEND_URL = "https://yandex.ru";
}
