package com.generationstock.email;

import com.generationstock.config.ApplicationConstants;
import org.springframework.stereotype.Component;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

@Component
public class EmailSender {

    // Отправляем письмо активации через Gmail
    public void sendActivationEmailFromGMail(String email, String userActivationCode, String password) {
        String from = "generationstock0";
        String pass = "6rC-bh5-4Nk-dKT";
        String subject = "Подтвердите ваш E-mail!";
        String body = "Здравствуйте!" +
                "<br><br>" +
                "Вы проходите регистрацию на сайте generation-stock.ru." +
                "<br><br>" +
                "Для подтверждения вашего E-mail-адреса перейдите по этой ссылке:" +
                "<br><br>" +
                "<a href=" + ApplicationConstants.BACKEND_URL + "/activation/" + email + "/" + userActivationCode + ">Подтвердить E-mail</a>" +
                "<br><br>" +
                "Ваши данные для входа в личный кабинет:" +
                "<br><br>" +
                "E-mail: " + email +
                "<br>" +
                "Пароль: " + password +
                "<br><br>" +
                "С уважением," +
                "<br>" +
                "команда проекта generation-stock.ru";
        String[] to = {email};
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom("GenerationStock");
//            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
//            message.setText(body); // для отправки обычного текста
            message.setContent(body, "text/html; charset=utf-8"); // для отправки HTML
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }

    // ПОВТОРНО отправляем письмо активации через Gmail
    public void retrySendActivationEmailFromGMail(String email, String userActivationCode) {
        String from = "generationstock0";
        String pass = "6rC-bh5-4Nk-dKT";
        String subject = "Подтвердите ваш E-mail!";
        String body = "Здравствуйте!" +
                "<br><br>" +
                "Вы получили это письмо, так ваш адрес электронной почты на сайте generation-stock.ru " +
                "до сих пор не активирован." +
                "<br><br>" +
                "Для подтверждения вашего E-mail-адреса перейдите по этой ссылке:" +
                "<br><br>" +
                "<a href=" + ApplicationConstants.BACKEND_URL + "/activation/" + email + "/" + userActivationCode + ">Подтвердить E-mail</a>" +
                "<br><br>" +
                "С уважением," +
                "<br>" +
                "команда проекта generation-stock.ru";
        String[] to = {email};
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom("GenerationStock");
//            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
//            message.setText(body); // для отправки обычного текста
            message.setContent(body, "text/html; charset=utf-8"); // для отправки HTML
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }

    // Отправляем письмо восстановления пароля через Gmail
    public void sendPasswordRecoveryEmailFromGMail(String email, String userRecoveryPasswordCode) {
        String from = "generationstock0";
        String pass = "6rC-bh5-4Nk-dKT";
        String subject = "Восстановление пароля";
        String body = "Здравствуйте!" +
                "<br><br>" +
                "Вы проходите процедуру восстановления пароля на сайте generation-stock.ru." +
                "<br><br>" +
                "Для смены пароля перейдите по этой ссылке:" +
                "<br><br>" +
                "<a href=" + ApplicationConstants.BACKEND_URL + "/recovery/" + email + "/" + userRecoveryPasswordCode + ">Сменить пароль</a>" +
                "<br><br>" +
                "С уважением," +
                "<br>" +
                "команда проекта generation-stock.ru";
        String[] to = {email};
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom("GenerationStock");
//            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
//            message.setText(body); // для отправки обычного текста
            message.setContent(body, "text/html; charset=utf-8"); // для отправки HTML
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }

    // Отправляем письмо с новым (восстановленным) паролем через Gmail
    public void sendNewPasswordFromGMail(String email, String password) {
        String from = "generationstock0";
        String pass = "6rC-bh5-4Nk-dKT";
        String subject = "Новый пароль!";
        String body = "Здравствуйте!" +
                "<br><br>" +
                "Вы подтвердили смену пароля на сайте generation-stock.ru." +
                "<br><br>" +
                "Ваши новые данные для входа в личный кабинет:" +
                "<br><br>" +
                "E-mail: " + email +
                "<br>" +
                "Пароль: " + password +
                "<br><br>" +
                "Войти на сайт по новым данным вы можете на странице авторизации:" +
                "<br><br>" +
                "<a href=" + ApplicationConstants.FRONTEND_URL + "/login>Войти на сайт</a>" +
                "<br><br>" +
                "С уважением," +
                "<br>" +
                "команда проекта generation-stock.ru";
        String[] to = {email};
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom("GenerationStock");
//            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
//            message.setText(body); // для отправки обычного текста
            message.setContent(body, "text/html; charset=utf-8"); // для отправки HTML
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }
}
