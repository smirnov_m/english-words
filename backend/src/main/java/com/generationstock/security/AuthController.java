package com.generationstock.security;

import com.generationstock.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Transactional
public class AuthController {

    @Autowired
    private UserService userService;

    // Авторизация пользователя
    @PostMapping("/login")
    public ResponseEntity<Object> authenticateUser(@RequestBody LoginRequest loginRequest) {
        return userService.authenticateUser(loginRequest);
    }

    // Регистрация пользователя
    @PostMapping("/reg")
    public ResponseEntity registerUser(@RequestBody SignUpRequest signUpRequest) {
        return userService.createUser(signUpRequest);
    }

}
