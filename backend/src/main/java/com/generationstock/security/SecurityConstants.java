package com.generationstock.security;

public class SecurityConstants {
    public static final String SIGN_UP_URLS = "/auth/*";
    public static final String EMAIL_ACTIVATION_URLS = "/activation/**";
    public static final String PASSWORD_RECOVERY_URLS = "/recovery/**";
    public static final String SECRET = "SecretKeyGetJWT";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String CONTENT_TYPE = "application/json;charset=UTF-8";
    public static final long EXPIRATION_TIME = 600_000; // в миллисекундах: 600_000 = 10 минут
}
