package com.generationstock.security;

import lombok.Data;

@Data
public class SignUpRequest {
    private String email;
}
