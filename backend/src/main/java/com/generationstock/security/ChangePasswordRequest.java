package com.generationstock.security;

import lombok.Data;

@Data
public class ChangePasswordRequest {
    private String password;
}
