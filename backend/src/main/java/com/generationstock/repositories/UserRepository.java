package com.generationstock.repositories;

import com.generationstock.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUserID(Long userID);
    Optional<User> findByUserEmail(String userEmail);
    void deleteUserByUserID(Long userID);


}
