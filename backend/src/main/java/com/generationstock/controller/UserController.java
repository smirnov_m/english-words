package com.generationstock.controller;

import com.generationstock.entities.User;
import com.generationstock.security.ChangePasswordRequest;
import com.generationstock.security.SignUpRequest;
import com.generationstock.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping
@RequiredArgsConstructor
@Transactional
public class UserController {
    @Autowired
    private UserService userService;

    // Получить пользователя по ID
    @GetMapping("/user/{userID}")
    public User getUserByID(@PathVariable Long userID) {
        return userService.getUserByID(userID);
    }

    // Получить список всех пользователей
//    @GetMapping("/user")
//    public List<User> getAllUsers() {
//        return userService.getAllUsers();
//    }

    // Удалить пользователя по ID
//    @DeleteMapping("/user/{userID}")
//    public void delete(@PathVariable Long userID) {
//        userService.deleteUserByID(userID);
//    }

    // Восстановление пароля пользователя
    @PostMapping("/recovery")
    public ResponseEntity userPasswordRecovery(@RequestBody SignUpRequest recoveryRequest) {
        return userService.userPasswordRecovery(recoveryRequest);
    }

    // Смена пароля из личного кабинета
    @PostMapping("/change_password")
    public ResponseEntity userChangePassword(HttpServletRequest request, @RequestBody ChangePasswordRequest password) {
        return userService.userChangePassword(request, password);
    }
}
