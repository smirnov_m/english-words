package com.generationstock.controller;

import com.generationstock.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping
@RequiredArgsConstructor
@Transactional
public class EmailController {

    @Autowired
    private UserService userService;

    // Проверка ссылки из письма активации
    @GetMapping("/activation/{userEmail}/{userActivationCode}")
    public ResponseEntity checkUserActivationCode(@PathVariable("userEmail") String userEmail,
                                                  @PathVariable("userActivationCode") String userActivationCode) {
        return userService.checkUserActivationCode(userEmail, userActivationCode);
    }

    // Проверка ссылки из письма восстановления пароля
    @GetMapping("/recovery/{userEmail}/{userRecoveryPasswordCode}")
    public ResponseEntity checkUserRecoveryPasswordCode(@PathVariable("userEmail") String userEmail,
                                                  @PathVariable("userRecoveryPasswordCode") String userRecoveryPasswordCode) {
        return userService.checkUserRecoveryPasswordCode(userEmail, userRecoveryPasswordCode);
    }

}
