package com.generationstock.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    private int projectID;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "project_deadline")
    private Timestamp projectDeadline;

    @Column(name = "project_color_code")
    private String projectColorCode;

    @Column(name = "user_id")
    private int userID;
}
