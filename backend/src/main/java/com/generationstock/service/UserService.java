package com.generationstock.service;

import com.generationstock.config.ApplicationConstants;
import com.generationstock.entities.Role;
import com.generationstock.entities.User;
import com.generationstock.security.*;
import com.generationstock.repositories.UserRepository;
import com.generationstock.email.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private JWTTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    // Регистрация нового пользователя
    public ResponseEntity createUser(SignUpRequest userIn) {

        // Проверяем, есть ли уже пользователь с таким E-mail в базе и активирован ли он
        if (getUserByEmail(userIn.getEmail()) != null && getUserByEmail(userIn.getEmail()).isUserIsAccepted() == false) {
            emailSender.retrySendActivationEmailFromGMail(userIn.getEmail(),
                    getUserByEmail(userIn.getEmail()).getUserActivationCode());
            return new ResponseEntity(new String[]{"Пользователь с таким E-mail уже существует, но учётная запись не активирована! На почту "
                    + userIn.getEmail() + " повторно отправлено письмо активации."}, HttpStatus.OK);
        }
        if (getUserByEmail(userIn.getEmail()) != null && getUserByEmail(userIn.getEmail()).isUserIsAccepted() == true) {
            return new ResponseEntity(new String[]{"Пользователь с таким E-mail уже существует! Войдите в личный кабинет по вашим" +
                    " данным, или пройдите процедуру восстановления пароля."}, HttpStatus.OK);
        }

        // Если нет, то создаём нового
        User user = new User();
        String password = generateUserActivationCode(6);

        user.setUserEmail(userIn.getEmail());
        user.setUserPassword(bCryptPasswordEncoder.encode(password));
        user.setUserActivationCode(generateUserActivationCode(10));

        List<Role> roles = new ArrayList<>();
        roles.add(new Role(2, "user")); // по умолчанию устанавливаем роль 2 - user (роль должна быть предварительно создана в таблице roles!!!)
        user.setRoles(roles);

        try {
            userRepository.save(user);
            emailSender.sendActivationEmailFromGMail(userIn.getEmail(), user.getUserActivationCode(), password);
            return new ResponseEntity(new String[]{"Пользователь создан"}, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity(new String[]{"Ошибка при сохранении учётной записи"}, HttpStatus.OK);
        }
    }

    // Авторизация пользователя
    public ResponseEntity authenticateUser(LoginRequest loginRequest) {

        // Проверяем, есть ли уже пользователь с таким E-mail в базе и активирован ли он
        if (getUserByEmail(loginRequest.getUsername()) != null && getUserByEmail(loginRequest.getUsername()).isUserIsAccepted() == false) {
            emailSender.retrySendActivationEmailFromGMail(loginRequest.getUsername(),
                    getUserByEmail(loginRequest.getUsername()).getUserActivationCode());
            return new ResponseEntity(new String[]{"Пользователь не активирован!"}, HttpStatus.OK);
        }

        // Если да, то проверяем данные авторизации
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
        ));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = SecurityConstants.TOKEN_PREFIX + jwtTokenProvider.generatedToken(authentication);
        return ResponseEntity.ok(new JWTTokenSuccessResponse(true, jwt));
    }

    // Редактирование данных пользователя
    // todo Нужно переделать по образцу варианта с созданием пользователя, в частности сделать пункт для перекодировки пароля,  если он изменится
//    public void editUser(User user) {
//        userRepository.save(user);
//    }

    // Проверка ссылки из письма активации
    public ResponseEntity checkUserActivationCode(String userEmail, String userActivationCode) {
        User user = getUserByEmail(userEmail);
        if (user != null) {
            if (user.getUserActivationCode().equals(userActivationCode)) {
                user.setUserIsAccepted(true);
                return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(ApplicationConstants.FRONTEND_URL + "/#/success")).build();
            } else {
                return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(ApplicationConstants.FRONTEND_URL + "/#/error")).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(ApplicationConstants.FRONTEND_URL + "/#/error")).build();
        }
    }

    // Проверка ссылки из письма восстановления пароля
    public ResponseEntity checkUserRecoveryPasswordCode(String userEmail, String userRecoveryPasswordCode) {
        User user = getUserByEmail(userEmail);
        if (user != null) {
            if (user.getUserRecoveryPasswordCode().equals(userRecoveryPasswordCode)) {
                String password = generateUserActivationCode(6);
                user.setUserPassword(bCryptPasswordEncoder.encode(password));
                emailSender.sendNewPasswordFromGMail(userEmail, password);
                return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(ApplicationConstants.FRONTEND_URL + "/#/changepassword")).build();
            } else {
                return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(ApplicationConstants.FRONTEND_URL + "#/error")).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(ApplicationConstants.FRONTEND_URL + "/#/error")).build();
        }
    }

    // Удаление пользователя
//    public void deleteUserByID(Long userID) {
//        userRepository.deleteUserByUserID(userID);
//    }

    // Получение пользователя по ID
    public User getUserByID(Long userID) {
        Optional<User> user = userRepository.findByUserID(userID);
        if (user.isPresent()) {
            return user.get();
        } else {
            return null;
        }
    }

    // Получение пользователя по E-mail
    public User getUserByEmail(String userEmail) {
        Optional<User> user = userRepository.findByUserEmail(userEmail);
        if (user.isPresent()) {
            return user.get();
        } else {
            return null;
        }
    }

    // Получение всех пользователей
//    public List<User> getAllUsers() {
//        return userRepository.findAll();
//    }

    // Генерируем код активации для ссылки активации
    public String generateUserActivationCode(int length) {
        String characters = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOASDFGHJKLZXCVBNM";
        Random rnd = new Random();
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(rnd.nextInt(characters.length()));
        }
        return new String(text);
    }

    // Восстановление пароля пользователя
    public ResponseEntity userPasswordRecovery(SignUpRequest recoveryRequest) {

        // Проверяем, сущствует ли такой пользователь
        if (getUserByEmail(recoveryRequest.getEmail()) == null) {
            return new ResponseEntity(new String[]{"Пользователь не существует!"}, HttpStatus.OK);
        }

        // Если существует, то активирован ли пользователь
        if (getUserByEmail(recoveryRequest.getEmail()) != null && getUserByEmail(recoveryRequest.getEmail()).isUserIsAccepted() == false) {
            emailSender.retrySendActivationEmailFromGMail(recoveryRequest.getEmail(),
                    getUserByEmail(recoveryRequest.getEmail()).getUserActivationCode());
            return new ResponseEntity(new String[]{"Пользователь не активирован!"}, HttpStatus.OK);
        }

        // Если существует и активирован, то отправляем ссылку для перехода на страницу изменения пароля
        String recoveryCode = generateUserActivationCode(6);
        getUserByEmail(recoveryRequest.getEmail()).setUserRecoveryPasswordCode(recoveryCode);
        emailSender.sendPasswordRecoveryEmailFromGMail(recoveryRequest.getEmail(),
                recoveryCode);
        return new ResponseEntity(new String[]{"Отправлено письмо для восстановления пароля!"}, HttpStatus.OK);
    }

    // Смена пароля из личного кабинета
    public ResponseEntity userChangePassword(HttpServletRequest request, ChangePasswordRequest password) {
        String jwt = getJWTFromRequest(request);
        Long userId = jwtTokenProvider.getUserIdFromToken(jwt);
        if (getUserByID(userId) != null) {
            getUserByID(userId).setUserPassword(bCryptPasswordEncoder.encode(password.getPassword()));
            return new ResponseEntity(new String[]{"Пароль изменён!"}, HttpStatus.OK);
        } else {
            return new ResponseEntity(new String[]{"Произошла ошибка!"}, HttpStatus.OK);
        }
    }

    // Получение JWT-токена из запроса
    private String getJWTFromRequest(HttpServletRequest request) {
        String bearToken = request.getHeader(SecurityConstants.HEADER_STRING);
        if (StringUtils.hasText(bearToken) && bearToken.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            return bearToken.split(" ")[1];
        }
        return null;
    }
}
