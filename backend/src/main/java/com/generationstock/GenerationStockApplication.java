package com.generationstock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenerationStockApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenerationStockApplication.class, args);
	}

}
