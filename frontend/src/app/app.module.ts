import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatBadgeModule} from '@angular/material/badge';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from "@angular/material/input";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatCardModule} from "@angular/material/card";
import {MatSelectModule} from "@angular/material/select";
import {HttpClientModule} from "@angular/common/http";
import {MatDialogModule} from "@angular/material/dialog";
import {APP_CONFIG} from "./tokens/config-token";
import {CommonModule} from "@angular/common";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {authInterceptorProviders} from "./helper/auth-interceptor.service";
import {authErrorInterceptorProvider} from "./helper/error-interceptor.service";
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { NavigationComponent } from './layout/navigation/navigation.component';
import {MatMenuModule} from "@angular/material/menu";
import { FooterComponent } from './layout/footer/footer.component';
import {SuccessRegistrationComponent} from "./auth/success/success-registration.component";
import {ConfirmRegistrationComponent} from "./auth/confirm/confirm-registration.component";
import {RecoveryComponent} from "./auth/recovery/recovery.component";
import {ErrorRegistrationComponent} from "./auth/error/error-registration.component";
import {ChangePasswordComponent} from "./auth/changepassword/change-password.component";
import { MainComponent } from './pages/main/main.component';
import {GameComponent} from "./pages/game/game.component";
import {ProfileComponent} from "./pages/profile/profile.component";
import {ResultsComponent} from "./pages/results/results.component";
import {RememberComponent} from "./pages/remember/remember.component";
import { EnglishComponent } from '../assets/english/english.component';
import { RussianComponent } from '../assets/russian/russian.component';
import {AboutComponent} from "./pages/about/about.component";
import {CheckComponent} from "./pages/check/check.component";
import {MemoryResultComponent} from "./pages/memory-result/memory-result.component";
import {RememberResultsComponent} from "./pages/remember-results/remember-results.component";
import {GameModeComponent} from "./pages/game-mode/game-mode.component";

@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    LoginComponent,
    RegisterComponent,
    NavigationComponent,
    FooterComponent,
    SuccessRegistrationComponent,
    ConfirmRegistrationComponent,
    RecoveryComponent,
    ErrorRegistrationComponent,
    ChangePasswordComponent,
    MainComponent,
    GameComponent,
    ProfileComponent,
    ResultsComponent,
    RememberComponent,
    EnglishComponent,
    RussianComponent,
    AboutComponent,
    CheckComponent,
    MemoryResultComponent,
    RememberResultsComponent,
    GameModeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatGridListModule,
    MatTableModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatSelectModule,
    HttpClientModule,
    MatDialogModule,
    CommonModule,
    MatSnackBarModule,
    MatMenuModule
  ],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: {
        BACKEND_LINK: 'http://localhost:8080/'
        // BACKEND_LINK: 'http://'
      }
    },
    authInterceptorProviders,
    authErrorInterceptorProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
