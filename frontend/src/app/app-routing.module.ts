import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./auth/login/login.component";
import {RegisterComponent} from "./auth/register/register.component";
import {AuthGuardService} from "./helper/auth-guard.service";
import {MyNavComponent} from "./my-nav/my-nav.component";
import {SuccessRegistrationComponent} from "./auth/success/success-registration.component";
import {ConfirmRegistrationComponent} from "./auth/confirm/confirm-registration.component";
import {RecoveryComponent} from "./auth/recovery/recovery.component";
import {ErrorRegistrationComponent} from "./auth/error/error-registration.component";
import {ChangePasswordComponent} from "./auth/changepassword/change-password.component";
import {MainComponent} from "./pages/main/main.component";
import {GameComponent} from "./pages/game/game.component";
import {ResultsComponent} from "./pages/results/results.component";
import {RememberComponent} from "./pages/remember/remember.component";
import {AboutComponent} from "./pages/about/about.component";
import {ProfileComponent} from "./pages/profile/profile.component";
import {CheckComponent} from "./pages/check/check.component";
import {MemoryResultComponent} from "./pages/memory-result/memory-result.component";
import {RememberResultsComponent} from "./pages/remember-results/remember-results.component";
import {GameModeComponent} from "./pages/game-mode/game-mode.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'reg', component: RegisterComponent},
  {path: 'success', component: SuccessRegistrationComponent},
  {path: 'confirm', component: ConfirmRegistrationComponent},
  {path: 'recovery', component: RecoveryComponent},
  {path: 'error', component: ErrorRegistrationComponent},
  {path: 'changepassword', component: ChangePasswordComponent},
  {path: 'main', component: MyNavComponent,
    children: [
      { path: '', component: MainComponent },
      { path: 'main', component: GameModeComponent },
      { path: 'game', component: GameComponent },
      { path: 'results', component: ResultsComponent },
      { path: 'remember', component: RememberComponent },
      { path: 'memoryresult', component: MemoryResultComponent },
      { path: 'check', component: CheckComponent },
      { path: 'rememberresults', component: RememberResultsComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'about', component: AboutComponent },
    ]},
  // {path: 'main', component: MyNavComponent, canActivate: [AuthGuardService]},
  {path: '', redirectTo: 'main/main', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
