import {Component, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from "@angular/animations";
import {EnglishComponent} from "../../../assets/english/english.component";
import {RussianComponent} from "../../../assets/russian/russian.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-remember',
  templateUrl: './remember.component.html',
  styleUrls: ['./remember.component.css'],
  animations: [
    trigger('start', [
      transition('void => *', [
        style({opacity: 1, display: "inline"}),
        animate('800ms ease-out')
      ])
    ])
  ]
})
export class RememberComponent implements OnInit {

  countButtonVisible = true; // переключатель видимости обратного отсчета
  playButtonVisible = true; // переключатель видимости кнопки "Старт"
  trueButtonVisible = false; // переключатель видимости зелёной галочки
  falseButtonVisible = false; // переключатель видимости красного крестика
  count = 0; // отсчет в начале раунда
  public counter = 60; // количество секунд в раунде
  englishWords: string[] = EnglishComponent.englishWords; // заполняем массив русских слов
  russianWords: string[] = RussianComponent.russianWords; // заполняем массив английских слов
  indexOfRightWord = 0; // индекс в массиве со словами
  englishWord = ""; // английское слово на карточке
  static dailyAttempts = 5; // количество дневных попыток
  static points = 0; // количество набранных баллов (для передачи на экран результатов)
  static rememberedWords: number[] = []; // массив запомненных слов (индексы из базы слов)
  numberOfRememberedWords = 0; // количество запомненных слов

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    RememberComponent.rememberedWords = [];
    this.initWords();
  }

  // Генератор случайного индекса в массиве со словами
  randomIndex() {
    return Math.floor((Math.random() * this.englishWords.length));
  }

  // Таймер обратного отсчета времени раунда
  timer() {
    let intervalId = setInterval(() => {
      this.counter = this.counter - 1;
      if(this.counter === 0) {
        clearInterval(intervalId)
      }
    }, 1000)
  }

  // Инициализация всех слов
  initWords() {
    this.indexOfRightWord = this.randomIndex();
    this.englishWord = this.englishWords[this.indexOfRightWord];
  }

  // Проверка правильного ответа
  check(indexOfRightWord: number) {
    RememberComponent.rememberedWords.push(indexOfRightWord);
    this.numberOfRememberedWords++;
    if (this.counter == 0) {
      this.router.navigate(['main/memoryresult']);
    }
    this.initWords();
  }

  // Таймер обратного отсчета при запуске игры
  countDown() {
    RememberComponent.dailyAttempts--;
    RememberComponent.points = 0;
    this.playButtonVisible = !this.playButtonVisible;
    this.count = 5;
    setTimeout(() => {
        this.count = 4;
      },
      1000);
    setTimeout(() => {
        this.count = 3;
      },
      2000);
    setTimeout(() => {
        this.count = 2;
      },
      3000);
    setTimeout(() => {
        this.count = 1;
      },
      4000);
    setTimeout(() => {
        this.count = 0;
        this.countButtonVisible = !this.countButtonVisible;
        this.timer();
      },
      4800);
  }
}
