import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {AuthService} from "../../service/auth.service";
import {TokenStorageService} from "../../service/token-storage.service";
import {NotificationService} from "../../service/notification.service";
import {Router} from "@angular/router";
import {Subject} from "rxjs";
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public profileForm!: FormGroup;

  hide = true;

  updateData$: Subject<boolean> = new BehaviorSubject<boolean>(false);

  private checkPasswords: ValidatorFn = (group: AbstractControl):  ValidationErrors | null => {
    // @ts-ignore
    let pass = group.get('password').value;
    // @ts-ignore
    let confirmPass = group.get('confirmPassword').value

    return pass === confirmPass ? null : { notSame: true }
  };

  constructor(
    private authService: AuthService,
    private notificationService: NotificationService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.profileForm = this.createLoginForm()
  }

  public createLoginForm(): FormGroup {
    return this.fb.group({
      password: ['', Validators.compose([Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.required])],
    }, {validators: this.checkPasswords})
  }

  public submit(): void {
    this.updateData$.next(true);
    this.authService.changePassword({
      userPassword: this.profileForm.value.password,
    }).subscribe(data => {
      this.updateData$.next(false);
      this.notificationService.showSnackBar(data);
    });
  }

}
