import { Component, OnInit } from '@angular/core';
import {GameComponent} from "../game/game.component";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  dailyAttempts = GameComponent.dailyAttempts;

  constructor() { }

  ngOnInit(): void {
  }

}
