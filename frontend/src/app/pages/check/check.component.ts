import {Component, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from "@angular/animations";
import {EnglishComponent} from "../../../assets/english/english.component";
import {RussianComponent} from "../../../assets/russian/russian.component";
import {Router} from "@angular/router";
import {RememberComponent} from "../remember/remember.component";

@Component({
  selector: 'app-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.css'],
  animations: [
    trigger('start', [
      transition('void => *', [
        style({opacity: 1, display: "inline"}),
        animate('800ms ease-out')
      ])
    ])
  ]
})
export class CheckComponent implements OnInit {

  countButtonVisible = true; // переключатель видимости обратного отсчета
  playButtonVisible = true; // переключатель видимости кнопки "Старт"
  trueButtonVisible = false; // переключатель видимости зелёной галочки
  falseButtonVisible = false; // переключатель видимости красного крестика
  count = 0; // отсчет в начале раунда
  public counter = 60; // количество секунд в раунде
  englishWords: string[] = EnglishComponent.englishWords; // заполняем массив русских слов
  russianWords: string[] = RussianComponent.russianWords; // заполняем массив английских слов
  indexOfRightWord = 0; // индекс в массиве со словами
  positionOfRightWord = 0; // позиция правильного ответа
  englishWord = ""; // английское слово на карточке
  russianWord: string[] = []; // массив русских на карточке
  static dailyAttempts = 5; // количество дневных попыток
  static points = 0; // количество набранных баллов (для передачи на экран результатов)
  thisPagePoints = 0; // количество набранных баллов для (для этого экрана)
  unixTimeStart = 0; // Unix-time начала очередной карточки
  unixTimeEnd = 0; // Unix-time окончания очередной карточки
  rememberedWords: number[] = RememberComponent.rememberedWords; // массив запомненных слов (индексы из базы слов)
  positionOfRememberedWords = 0; // номер угадываемого слова

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.rememberedWords = this.shuffle(this.rememberedWords);
    this.initWords();
  }

  // Генератор случайного индекса в массиве со словами
  randomIndex() {
    return Math.floor((Math.random() * this.englishWords.length));
  }

  // Генератор случайной позиции правильного ответа
  randomPosition() {
    return Math.floor((Math.random() * 4) + 1);
  }

  // Таймер обратного отсчета времени раунда
  timer() {
    let intervalId = setInterval(() => {
      this.counter = this.counter - 1;
      if(this.counter === 0) {
        clearInterval(intervalId)
      }
    }, 1000)
  }

  // Инициализация всех слов
  initWords() {
    this.indexOfRightWord = this.rememberedWords[this.positionOfRememberedWords];
    this.positionOfRightWord = this.randomPosition();
    this.englishWord = this.englishWords[this.indexOfRightWord];
    this.russianWord[1] = this.russianWords[this.randomIndex()];
    this.russianWord[2] = this.russianWords[this.randomIndex()];
    this.russianWord[3] = this.russianWords[this.randomIndex()];
    this.russianWord[4] = this.russianWords[this.randomIndex()];
    this.russianWord[this.positionOfRightWord] = this.russianWords[this.indexOfRightWord];
  }

  // Проверка правильного ответа
  check(cardNumber: number) {
    this.unixTimeEnd = this.getUnixTime();
    if (this.russianWord[cardNumber] == this.russianWords[this.indexOfRightWord]) {
      CheckComponent.points += Math.floor(100 / ((this.unixTimeEnd - this.unixTimeStart) / 1000)); // формула рассчета баллов
      this.thisPagePoints = CheckComponent.points;
      this.trueButtonVisible = !this.trueButtonVisible;
      setTimeout(() => {
          this.trueButtonVisible = !this.trueButtonVisible;
        },
        800);
    } else {
      this.falseButtonVisible = !this.falseButtonVisible;
      setTimeout(() => {
          this.falseButtonVisible = !this.falseButtonVisible;
        },
        800);
    }
    this.positionOfRememberedWords++;
    this.unixTimeStart = this.getUnixTime();
    if (this.counter == 0) {
      this.router.navigate(['main/results']);
    }
    if (this.positionOfRememberedWords == this.rememberedWords.length) {
      this.router.navigate(['main/results']);
    }
    this.initWords();
  }

  // Таймер обратного отсчета при запуске игры
  countDown() {
    CheckComponent.dailyAttempts--;
    CheckComponent.points = 0;
    this.playButtonVisible = !this.playButtonVisible;
    this.count = 5;
    setTimeout(() => {
        this.count = 4;
      },
      1000);
    setTimeout(() => {
        this.count = 3;
      },
      2000);
    setTimeout(() => {
        this.count = 2;
      },
      3000);
    setTimeout(() => {
        this.count = 1;
      },
      4000);
    setTimeout(() => {
        this.count = 0;
        this.countButtonVisible = !this.countButtonVisible;
        this.timer();
        this.unixTimeStart = this.getUnixTime();
      },
      4800);
  }

  // Получение Unix-time в миллисекундах
  getUnixTime() {
    return Math.floor(Date.now());
  }

  // Перетасовка массива запомненных слов
  shuffle(array: number[]) {
    for (let i = array.length -1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }
}
