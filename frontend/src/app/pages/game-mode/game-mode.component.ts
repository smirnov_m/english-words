import {Component, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from "@angular/animations";
import {EnglishComponent} from "../../../assets/english/english.component";
import {RussianComponent} from "../../../assets/russian/russian.component";
import {Router} from "@angular/router";
import {RememberComponent} from "../remember/remember.component";

@Component({
  selector: 'app-memory-result',
  templateUrl: './game-mode.component.html',
  styleUrls: ['./game-mode.component.css'],
  animations: [
    trigger('start', [
      transition('void => *', [
        style({opacity: 1, display: "inline"}),
        animate('800ms ease-out')
      ])
    ])
  ]
})
export class GameModeComponent implements OnInit {

  rememberedWords: number[] = RememberComponent.rememberedWords; // массив запомненных слов (индексы из базы слов)

  constructor() {
  }

  ngOnInit(): void {
  }
}
