import { Component, OnInit } from '@angular/core';
import {GameComponent} from "../game/game.component";

@Component({
  selector: 'app-remember-results',
  templateUrl: './remember-results.component.html',
  styleUrls: ['./remember-results.component.css']
})
export class RememberResultsComponent implements OnInit {

  dailyAttempts = GameComponent.dailyAttempts;
  points = GameComponent.points;

  constructor() { }

  ngOnInit(): void {
  }

  // Проверка и запись лучших результатов
  topResults() {

  }

  // Проверка и повышение уровня
  level() {

  }

}
