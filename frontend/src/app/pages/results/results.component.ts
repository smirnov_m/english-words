import { Component, OnInit } from '@angular/core';
import {GameComponent} from "../game/game.component";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  dailyAttempts = GameComponent.dailyAttempts;
  points = GameComponent.points;

  constructor() { }

  ngOnInit(): void {
  }

  // Проверка и запись лучших результатов
  topResults() {

  }

  // Проверка и повышение уровня
  level() {

  }

}
