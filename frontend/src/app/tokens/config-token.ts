import {InjectionToken} from "@angular/core";

export interface ConfigInterface {
  BACKEND_LINK: string;
}

export const APP_CONFIG = new InjectionToken<ConfigInterface>('My Application Config');
