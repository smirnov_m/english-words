import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../model/user";
import {Observable} from "rxjs";
import {APP_CONFIG, ConfigInterface} from "../tokens/config-token";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: ConfigInterface) { }

  // Авторизация
  public login(user: { userEmail: any; userPassword: any; }): Observable<any> {
    return this.http.post(this.config.BACKEND_LINK + 'auth/login', {
      username: user.userEmail,
      password: user.userPassword
    });
  }

  // Регистрация
  public register(user: { email: any; }): Observable<any> {
    return this.http.post(this.config.BACKEND_LINK + 'auth/reg', user);
  }

  // Восстановление пароля
  public recovery(user: { email: any; }): Observable<any> {
    return this.http.post(this.config.BACKEND_LINK + 'recovery', user);
  }

  // Смена пароля
  public changePassword(user: { userPassword: any; }): Observable<any> {
    return this.http.post(this.config.BACKEND_LINK + 'change_password', {
      password: user.userPassword
    });
  }
}
