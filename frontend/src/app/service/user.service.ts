import {Inject, Injectable} from '@angular/core';
import {APP_CONFIG, ConfigInterface} from "../tokens/config-token";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../model/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: ConfigInterface) { }

  public getUserById(id: number): Observable<any> {
    return this.http.get(this.config.BACKEND_LINK + 'user/' + id);
  }

  public getCurrentUser(): Observable<any> {
    return this.http.get(this.config.BACKEND_LINK + 'user')
  }

  public updateUser(id: number, user: User): Observable<any> {
    return this.http.patch(this.config.BACKEND_LINK + 'user/' + id, user);
  }
}
