import {Component, OnInit, Inject} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {APP_CONFIG, ConfigInterface} from "../tokens/config-token";
import {TokenStorageService} from "../service/token-storage.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent implements OnInit {

  showTextMenu = !true; // отображение текста пунктов меню в свёрнутом сайдбаре (только значки)

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private http: HttpClient,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              @Inject(APP_CONFIG) private config: ConfigInterface,
              private tokenService: TokenStorageService,
              private router: Router) {

  }

  ngOnInit(): void {

  }

  public logout(): void {
    this.tokenService.logOut();
    this.router.navigate(['/login'])
  }
}
