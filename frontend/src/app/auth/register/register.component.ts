import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../service/auth.service';
import {TokenStorageService} from '../../service/token-storage.service';
import {NotificationService} from '../../service/notification.service';
import {Router} from "@angular/router";
import {BehaviorSubject, Subject} from "rxjs";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm!: FormGroup;

  updateData$: Subject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private router: Router,
    private notificationService: NotificationService,
    private fb: FormBuilder
  ) {
    if (this.tokenStorage.getUser()) {
      this.router.navigate(['main']);
    }
  }

  ngOnInit(): void {
    this.registerForm = this.createRegisterForm();
  }

  createRegisterForm(): FormGroup {
    return this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])]
    });
  }

  submit(): void {
    this.updateData$.next(true);
    this.authService.register({
      email: this.registerForm.value.email
    }).subscribe(data => {
      this.updateData$.next(false);
      if (data == "Пользователь создан") {
        this.router.navigate(['confirm']);
      } else {
        this.notificationService.showSnackBar(data);
      }
    });
  }

}
