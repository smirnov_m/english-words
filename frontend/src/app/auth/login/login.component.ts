import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../service/auth.service";
import {TokenStorageService} from "../../service/token-storage.service";
import {Router} from "@angular/router";
import {NotificationService} from "../../service/notification.service";
import {BehaviorSubject, Subject} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm!: FormGroup;

  hide = true;

  updateData$: Subject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private notificationService: NotificationService,
    private router: Router,
    private fb: FormBuilder
  ) {
    if (this.tokenStorage.getUser()) {
      this.router.navigate(['main']);
    }
  }

  ngOnInit(): void {
    this.loginForm = this.createLoginForm()
  }

  public createLoginForm(): FormGroup {
    return this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
    })
  }

  public submit(): void {
    this.updateData$.next(true);
    this.authService.login({
      userEmail: this.loginForm.value.username,
      userPassword: this.loginForm.value.password,
    }).subscribe(data => {
      this.updateData$.next(false);
      if (data == "Пользователь не активирован!") {
        this.notificationService.showSnackBar('Пользователь с таким E-mail уже существует, ' +
          'но учётная запись не активирована! На указанный E-mail повторно отправлено письмо активации.');
      } else {
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUser(data);
        this.notificationService.showSnackBar('Успешная авторизация!');
        this.router.navigate(['/main']);
      }
    }, error => {
      this.updateData$.next(false);
      this.notificationService.showSnackBar('Неправильные данные!');
    });
  }

}
