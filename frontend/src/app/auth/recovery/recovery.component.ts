import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../service/auth.service';
import {TokenStorageService} from '../../service/token-storage.service';
import {NotificationService} from '../../service/notification.service';
import {Router} from "@angular/router";
import {BehaviorSubject, Subject} from "rxjs";

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.css']
})
export class RecoveryComponent implements OnInit {

  public registerForm!: FormGroup;

  updateData$: Subject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private router: Router,
    private notificationService: NotificationService,
    private fb: FormBuilder
  ) {
    if (this.tokenStorage.getUser()) {
      this.router.navigate(['main']);
    }
  }

  ngOnInit(): void {
    this.registerForm = this.createRegisterForm();
  }

  createRegisterForm(): FormGroup {
    return this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])]
    });
  }

  submit(): void {
    this.updateData$.next(true);
    this.authService.recovery({
      email: this.registerForm.value.email
    }).subscribe(data => {
      this.updateData$.next(false);
      if (data == "Пользователь не активирован!") {
        this.notificationService.showSnackBar('Пользователь с таким E-mail уже существует, ' +
          'но учётная запись не активирована! На указанный E-mail повторно отправлено письмо активации.');
      } else {
        this.notificationService.showSnackBar(data);
      }
    });
  }

}
