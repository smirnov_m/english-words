export interface User {
  userID: number;
  userEmail: string;
  userFirstName: string;
  userLastName: string;
  userPassword: string;
  userIsAccepted: boolean;
  userActivationCode: number;
}
